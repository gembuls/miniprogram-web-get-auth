import './App.css';
import { useState } from 'react';
import CryptoJS from 'crypto-js';


function App() {

  const [token, setToken] = useState('');
  const [userAgent, setUserAgent] = useState();

  function handleClick() {
    const args = "Telkomsel"
    const auth = window.Android.getAuth();
    const data = JSON.parse(auth)
    const key = CryptoJS.enc.Utf8.parse('TelkomselACTION!'); // The same key used for encryption in Android
    const iv = CryptoJS.enc.Utf8.parse('TelkomselJuara#1'); // The same IV used for encryption in Android

    // Encrypted data received from Android app
    const encryptedData = data.token; // Replace this with the actual encrypted data

    // Decrypt the data
    const decryptedData = CryptoJS.AES.decrypt(encryptedData, key, { iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });

    // Convert the decrypted data to a readable format (e.g., string)
    const decryptedText = decryptedData.toString(CryptoJS.enc.Utf8);
    setToken(decryptedText);

    const param = window.navigator.userAgent
    setUserAgent(param)
  }

  function setStatusBarBackground() {
    window.Android.setStatusBarColorBackground('#FFFFFF')
  }

  function setStatusTheme() {
    const isLight = true
    window.Android.isStatusBarLightTheme(isLight)
  }

  return (
    <div className="App">
      <header className="App-header">
        <p>
          Token: {token}
        </p>
        <p>
          User Agent: {userAgent}
        </p>
        <button onClick={handleClick}>
          Get Token
        </button>
        <button onClick={setStatusBarBackground}>
          Set Status Bar Background
        </button>
        <button onClick={setStatusTheme}>
          Set Status Theme
        </button>
      </header>
    </div>
  );
}

export default App;
